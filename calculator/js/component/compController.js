(function(angular) {
  'use strict';
  angular.module('calcApp')
    .component('viewCalculator', {
      templateUrl: '../viewCalculator.html',
      bindings: {
        onCalcStandar: '<',
      },
      controller: ['$log','operationCalc', function ($log, calcModel) {
        const ctrl = this;
        ctrl.calculator = calcModel;
        ctrl.$onInit = () => {
        };

        ctrl.addNumber = (paramNumber) => {
          if(calcModel.currentNumber === '0') {
            calcModel.currentNumber = '';
            calcModel.currentDisplay = '';
          }
          calcModel.currentDisplay += paramNumber;
          calcModel.currentNumber += paramNumber;
        };

        ctrl.addOperation = (paramCharacter) => {
          calcModel.setOperation(paramCharacter);
        };

        ctrl.changeSign = () => {
          calcModel.getSignNumber();
        };

        ctrl.enterClicked = () => {
          if (calcModel.currentNumber ===''){
            calcModel.currentNumber = calcModel.result;
          }
          calcModel.calculate();
          calcModel.currentDisplay = calcModel.result;
        };

        ctrl.resetClicked = () => {
          calcModel.reset();
        };

        ctrl.clearNumber = () => {
          let str = String(calcModel.currentDisplay) || '';
          calcModel.currentDisplay = str.substring(0, str.length - 1);
          let resultS = `${String(calcModel.result).substring(0, str.length - 1)}` || '0';
          calcModel.result = Number(resultS);
          calcModel.currentNumber = `${calcModel.currentNumber.substring(0, str.length - 1)}` || '0';
        };

        ctrl.getSqrt = () => {
          calcModel.setSqrt();
        };

        ctrl.getPow = (operat) => {
          calcModel.setPow(operat);
        };

        ctrl.getPercent = () => {
          calcModel.setPercent();
        };

        ctrl.getMR = () => {
          calcModel.getMR();
        };

        ctrl.setMS = () => {
          calcModel.setMS();
        };

        ctrl.setMC = () => {
          calcModel.setMC();
        };

        ctrl.getSumM = () => {
          calcModel.getSumM();
        };

        ctrl.getRestM = () => {
          calcModel.getRestM();
        };
      }],
    });


})(window.angular);