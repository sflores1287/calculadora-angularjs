(function(angular) {
  'use strict';
  angular.module('calcApp')
    .service('operationCalc', function () {
      return {
        result: 0,
        operation: '',
        currentNumber: '0',
        currentDisplay: '',
        currentNumberMR: '',

        reset: function() {
          this.result = 0;
          this.operation = '';
          this.currentNumber = '0';
          this.currentDisplay = '' ;
        },

        setOperation: function(operationToSet) {
          this.operation = operationToSet;
          if(this.currentNumber === '0') {
            this.currentDisplay = '0';
          }

          this.currentDisplay += ' ' + this.operation + ' ';
          if (this.result === 0){
            this.result = Number(this.currentNumber);
          }
          this.currentNumber = '';
        },

        calculate: function() {
          switch(this.operation) {
          case 'x':
            this.result = this.result * Number(this.currentNumber);
            break;
          case '+':
            this.result = this.result + Number(this.currentNumber);
            break;
          case '-':
            this.result = this.result - Number(this.currentNumber);
            break;
          case '÷':
            this.result = this.result / Number(this.currentNumber);
            break;
          case '1/x':
            this.result = Math.pow(Number(this.result), 1/Number(this.currentNumber));
            break;
          }
        },
        getSignNumber: function() {
          if (this.result == 0){
            switch(Math.sign(Number(this.currentNumber))) {
            case 1:
              this.currentDisplay = `-${this.currentNumber}`;
              this.currentNumber = `-${this.currentNumber}`;
              break;
            case -1:
              this.currentDisplay = `${Number(this.currentNumber) * -1}`;
              this.currentNumber = `${Number(this.currentNumber) * -1}`;
              break;
            case 0:
              if( Object.is(Math.sign(Number(this.currentNumber)),0 )) {
                this.currentDisplay = `-${this.currentNumber}`;
                this.currentNumber = `-${this.currentNumber}`;
              } else {
                this.currentDisplay = '0';
                this.currentNumber = '0';
              }
              break;
            }
          } else {
            let str = this.currentDisplay;
            let stResult = `${str.split(this.operation)[0]}${this.operation} `;

            this.currentDisplay = stResult;

            switch(Math.sign(Number(this.currentNumber))) {
            case 1:
              this.currentDisplay += `-${this.currentNumber}`;
              this.currentNumber = `-${this.currentNumber}`;
              break;
            case -1:
              this.currentDisplay += `${Number(this.currentNumber) * -1}`;
              this.currentNumber = `${Number(this.currentNumber) * -1}`;
              break;
            case 0:
              if( Object.is(Math.sign(Number(this.currentNumber)),0 )) {
                this.currentDisplay += `-${this.currentNumber}`;
                this.currentNumber = `-${this.currentNumber}`;
              } else {
                this.currentDisplay += '0';
                this.currentNumber = '0';
              }
              break;
            }
          }
        },
        setSqrt: function(){
          let sqrtData = 0;
          if (this.result == 0){
            sqrtData = Math.sqrt(Number(this.currentNumber));
          } else {
            sqrtData = Math.sqrt(Number(this.result));
          }
          this.result = sqrtData;
          this.currentDisplay = sqrtData;
          this.currentNumber = '0';
        },
        setPow: function(opertator){
          let sqrtData = 0;
          switch(opertator) {
          case 'x²':
            if (this.result == 0){
              sqrtData = Math.pow(Number(this.currentNumber), 2);
            } else {
              sqrtData = Math.pow(Number(this.result), 2);
            }
            this.result = sqrtData;
            this.currentDisplay = sqrtData;
            this.currentNumber = '0';
            break;
          case '1/x':
            this.setOperation('1/x');
            break;
          }
        },
        setPercent: function(){
          let percentData = 0;
          if (this.result == 0){
            percentData = (Number(this.currentNumber) / 100);
          } else {
            percentData = (Number(this.result) / 100);
          }
          this.result = percentData;
          this.currentDisplay = percentData;
          this.currentNumber = '0';
        },
        setMS: function(){
          localStorage.setItem('memoryData', JSON.stringify({
            firstNumber: this.currentNumber,
          }));
        },
        getMR: function(){
          let sData = JSON.parse(localStorage.getItem('memoryData'));
          if (sData){
            this.currentNumber = sData.firstNumber;
          }
        },
        setMC: function(){
          localStorage.removeItem('memoryData');
          this.currentNumber = '0';
        },
        getSumM: function(){
          let sData = JSON.parse(localStorage.getItem('memoryData'));
          if (sData){
            this.result = this.result + Number(this.currentNumber);
          }
        },
        getRestM: function(){
          let sData = JSON.parse(localStorage.getItem('memoryData'));
          if (sData){
            this.result = this.result - Number(this.currentNumber);
          }
        }
      };
    });
})(window.angular);