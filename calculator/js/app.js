(function(angular) {
  'use strict';
  angular.module('calcApp', [])
    .controller('calcController',['$scope', function($scope){
      const ctrl = this;
      ctrl.$onInit = () => {
        ctrl.arrayCalc = [
          {column1: '7', column2: '8', column3: '9',column4: 'x'},
          {column1: '4', column2: '5', column3: '6',column4: '-'},
          {column1: '1', column2: '2', column3: '3',column4: '+'},
          {column1: '±', column2: '0', column3: '.',column4: '='},
        ];
      };
    }]);
})(window.angular);
