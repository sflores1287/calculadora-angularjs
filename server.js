var express = require('express');
var app = express();
app.use(express.static('calculator'));
app.get('/', function (req, res,next) {
  res.redirect('/');
});
app.listen(8089, 'localhost');
console.log('Calculator Server is Listening on port 8089');